package com.cervisium.webapp.security;

import org.junit.Ignore;
import org.junit.Test;
import org.mindrot.jbcrypt.BCrypt;

public class PasswordHashTest {

	/**
	 * @throws Exception
	 */
	@Test
	@Ignore
	public void testBCrypt() throws Exception {
		long start = System.currentTimeMillis();
		String hash = BCrypt.hashpw("965CourtJester", BCrypt.gensalt(12));
		long hashTime = System.currentTimeMillis() - start;
		System.out.println("Hashed in " + Long.toString(hashTime) + "ms " + hash);
		
		start = System.currentTimeMillis();
		BCrypt.checkpw("simplepass", hash);
		long checkTime = System.currentTimeMillis() - start;
		System.out.println("Checked password hash in " + Long.toString(checkTime) + "ms");
	}
}
