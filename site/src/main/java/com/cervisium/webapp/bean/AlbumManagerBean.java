package com.cervisium.webapp.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.Navigation;
import com.cervisium.webapp.bean.model.IntegerKeyedModel;
import com.cervisium.webapp.entity.Album;

@ViewScoped
@Named("albumManager")
public class AlbumManagerBean implements Serializable {
	public static final long serialVersionUID = 1l;
	
	@Inject @Cervisium private transient EntityManager em;
	@Inject private SessionState session;
	
	private List<Album> albums;
	
	public List<Album> getAlbums() { return albums; }
	
	@PostConstruct
	public void init() {
		albums = em.createQuery("SELECT a FROM Album a WHERE a.brewery.id = :breweryId ORDER BY a.addedTime DESC", Album.class)
			.setParameter("breweryId", session.getActiveBrewery().getId()).getResultList();
	}
}
