package com.cervisium.webapp.bean;

import java.io.Serializable;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.Navigation;
import com.cervisium.webapp.entity.NewsItem;

@ViewScoped
@Named("newsEditor")
public class NewsEditorBean implements Serializable {
	public static final long serialVersionUID = 1l;
	
	@Inject @Cervisium private transient Logger log;
	@Inject @Cervisium private transient EntityManager em;
	@Inject SessionState session;
	
	private Integer editItemId;
	private NewsItem editedItem;
	
	public Integer getEditItemId() { return editItemId; }
	public void setEditItemId(Integer editItemId) { this.editItemId = editItemId; }
	public NewsItem getEditedItem() { return editedItem; }
	public void setEditedItem(NewsItem editedItem) { this.editedItem = editedItem; }
	
	public void loadNewsItem() {
		if (editItemId > 0) {
			editedItem = em.find(NewsItem.class, editItemId);
			em.detach(editedItem);
		} else {
			editedItem = new NewsItem();
			if (session.getLoggedIn()) {
				editedItem.setBreweryId(session.getLoggedInUser().getBrewery().getId());
			}
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveNewsItem() {
		if (session.getLoggedIn() == false || session.getLoggedInUser().getPrivPostNews() == false) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "You are not logged in or do not have access to modify news items.", null));
			return;
		}
		
		if (editedItem.getId() == null) {
			em.persist(editedItem);
			Navigation.navigate("/news/edit?faces-redirect=true&id=" + Integer.toString(editedItem.getId()));
		} else {
			em.merge(editedItem);
		}
		
		FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage(FacesMessage.SEVERITY_INFO, "News Item Saved", null));
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteNewsItem() {
		if (session.getLoggedIn() == false || session.getLoggedInUser().getPrivPostNews() == false) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "You are not logged in or do not have access to modify news items.", null));
			return;
		}
		
		em.createQuery("DELETE FROM NewsItem ni WHERE ni.id = :id")
			.setParameter("id", editedItem.getId()).executeUpdate();
		Navigation.navigate("/news/manage");
	}
}
