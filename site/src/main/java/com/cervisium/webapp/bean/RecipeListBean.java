package com.cervisium.webapp.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.Navigation;
import com.cervisium.webapp.bean.model.IntegerKeyedModel;
import com.cervisium.webapp.entity.Recipe;

@Named("recipeList")
@RequestScoped
public class RecipeListBean implements Serializable {
	
	public static final long serialVersionUID = 1l;
	
	@Inject @Cervisium private transient EntityManager em;
	@Inject SessionState session;

	private List<Recipe> recipes;
	
	@PostConstruct
	public void init() {
		loadRecipes();
	}
	
	public List<Recipe> getRecipes() {
		return recipes;
	}
	
	protected void loadRecipes() {
		recipes = em.createQuery("SELECT r FROM Recipe r WHERE r.breweryId = :breweryId", Recipe.class)
			.setParameter("breweryId", session.getActiveBrewery().getId())
			.getResultList();
	}
}
