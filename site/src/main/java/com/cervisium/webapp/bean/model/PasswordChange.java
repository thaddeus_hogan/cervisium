package com.cervisium.webapp.bean.model;

import javax.validation.constraints.Size;

public class PasswordChange {

	private String oldPassword;
	private String newPassword;
	private String newPasswordConfirm;
	
	public String getOldPassword() {
		return oldPassword;
	}
	
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	@Size(min = 8, message = "New password must be at least 8 characters")
	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getNewPasswordConfirm() {
		return newPasswordConfirm;
	}

	public void setNewPasswordConfirm(String newPasswordConfirm) {
		this.newPasswordConfirm = newPasswordConfirm;
	}
}
