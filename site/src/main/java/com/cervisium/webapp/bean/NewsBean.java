package com.cervisium.webapp.bean;

import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.entity.NewsItem;

@RequestScoped
@Named("news")
public class NewsBean {

	@Inject @Cervisium private EntityManager em;
	@Inject private SessionState session;
	
	private List<NewsItem> newsItems;
	private Integer offset;
	private Integer nextOffset;
	private Boolean renderMoreLink = Boolean.FALSE;
	
	public List<NewsItem> getNewsItems() { return newsItems; }
	public void setNewsItems(List<NewsItem> newsItems) { this.newsItems = newsItems; }
	public Integer getOffset() { return offset; }
	public void setOffset(Integer offset) { this.offset = offset; }
	public Integer getNextOffset() { return nextOffset; }
	public void setNextOffset(Integer nextOffset) { this.nextOffset = nextOffset; }
	public Boolean getRenderMoreLink() { return renderMoreLink; }
	public void setRenderMoreLink(Boolean renderMoreLink) { this.renderMoreLink = renderMoreLink; }
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void loadNewsItems() {
		if (offset == null) { offset = 0; }
		
		newsItems = em.createQuery("SELECT ni FROM NewsItem ni WHERE ni.breweryId = :breweryId " +
			"ORDER BY ni.postDate DESC", NewsItem.class)
			.setParameter("breweryId", session.getActiveBrewery().getId())
			.setFirstResult(offset).setMaxResults(10).getResultList();
		
		nextOffset = offset + 10;
		
		if (newsItems.size() == 10) { renderMoreLink = Boolean.TRUE; }
	}
}
