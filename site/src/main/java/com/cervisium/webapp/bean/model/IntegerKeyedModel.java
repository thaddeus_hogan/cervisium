package com.cervisium.webapp.bean.model;

import java.util.HashMap;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.cervisium.webapp.entity.Formulation;
import com.cervisium.webapp.entity.IntegerKeyedEntity;

public class IntegerKeyedModel<T extends IntegerKeyedEntity> extends ListDataModel<T> implements SelectableDataModel<T> {

	private HashMap<Integer, T> keyedData;
	
	public IntegerKeyedModel(List<T> data) {
		super(data);
		keyedData = new HashMap<Integer, T>(data.size());
		for (T datum : data) { keyedData.put(datum.getId(), datum); }
	}
	
	@Override
	public T getRowData(String idStr) {
		return keyedData.get(Integer.parseInt(idStr));
	}
	
	public Object getRowKey(T item) {
		return item.getId();
	};
}
