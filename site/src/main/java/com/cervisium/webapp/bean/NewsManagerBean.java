package com.cervisium.webapp.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.Navigation;
import com.cervisium.webapp.bean.model.IntegerKeyedModel;
import com.cervisium.webapp.entity.NewsItem;

@ViewScoped
@Named("newsManager")
public class NewsManagerBean implements Serializable {
	public static final long serialVersionUID = 1l;
	
	@Inject @Cervisium private transient EntityManager em;
	@Inject private SessionState session;
	
	private List<NewsItem> newsItems;
	
	@PostConstruct
	public void init() {
		loadNewsItems();
	}
	
	public List<NewsItem> getNewsItems() { return newsItems; }
	
	protected void loadNewsItems() {
		newsItems = em.createQuery(
			"SELECT ni FROM NewsItem ni WHERE ni.breweryId = :breweryId ORDER BY ni.postDate DESC", NewsItem.class)
			.setParameter("breweryId", session.getActiveBrewery().getId())
			.getResultList();
	}
}
