package com.cervisium.webapp.bean.model;

import java.util.Date;

import com.cervisium.webapp.entity.BrewStage;

public class InventoryItem implements Comparable<InventoryItem> {

	private String name;
	private Integer brewingId;
	private Date dateBrew;
	private Date dateSecondary;
	private Date dateKeg;
	private Date dateBottle;
	private BrewStage stage;
	private Integer ageDays;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getBrewingId() {
		return brewingId;
	}
	
	public void setBrewingId(Integer brewingId) {
		this.brewingId = brewingId;
	}
	
	public Date getDateBrew() {
		return dateBrew;
	}
	
	public void setDateBrew(Date dateBrew) {
		this.dateBrew = dateBrew;
	}
	
	public Date getDateSecondary() {
		return dateSecondary;
	}
	
	public void setDateSecondary(Date dateSecondary) {
		this.dateSecondary = dateSecondary;
	}
	
	public Date getDateKeg() {
		return dateKeg;
	}
	
	public void setDateKeg(Date dateKeg) {
		this.dateKeg = dateKeg;
	}
	
	public Date getDateBottle() {
		return dateBottle;
	}
	
	public void setDateBottle(Date dateBottle) {
		this.dateBottle = dateBottle;
	}
	
	public BrewStage getStage() {
		return stage;
	}
	
	public void setStage(BrewStage stage) {
		this.stage = stage;
	}
	
	public Integer getAgeDays() {
		return ageDays;
	}
	
	public void setAgeDays(Integer ageDays) {
		this.ageDays = ageDays;
	}
	
	public String getImageFile() {
		switch (stage) {
			case PRIMARY: return "6carb.png";
			case SECONDARY: return "5carb.png";
			case KEG: return "keg.png";
			default: return "bottle.png";
		}
	}
	
	@Override
	public int compareTo(InventoryItem o) {
		int myPts = getStage().getOrderPoints();
		int oPts = o.getStage().getOrderPoints();
		
		if (getDateBrew().before(o.getDateBrew())) { oPts += 1; }
		else if (getDateBrew().after(o.getDateBrew())) { myPts += 1; }
		
		return oPts - myPts;
	}
}
