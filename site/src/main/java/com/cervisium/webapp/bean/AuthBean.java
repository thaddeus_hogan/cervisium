package com.cervisium.webapp.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.hibernate.validator.constraints.Email;
import org.mindrot.jbcrypt.BCrypt;

import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.bean.model.PasswordChange;
import com.cervisium.webapp.entity.BreweryUser;

@ViewScoped
@Named("auth")
public class AuthBean implements Serializable {
	public static final long serialVersionUID = 1l;
	
	@Inject @Cervisium private transient Logger log;
	@Inject @Cervisium private transient EntityManager em;
	@Inject private SessionState session;
	
	private String email;
	private String password;
	private PasswordChange passwordChange;
	private UIComponent authForm;
	private UIComponent passwordChangeForm;
	private UIComponent newPasswordConfirm;
	
	@PostConstruct
	public void init() {
		passwordChange = new PasswordChange();
	}
	
	@Email(message = "Must be a valid email address")
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@NotNull(message = "Please enter your password")
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public UIComponent getAuthForm() {
		return authForm;
	}
	
	public void setAuthForm(UIComponent authForm) {
		this.authForm = authForm;
	}
	
	public PasswordChange getPasswordChange() {
		return passwordChange;
	}
	
	public void setPasswordChange(PasswordChange passwordChange) {
		this.passwordChange = passwordChange;
	}
	
	public UIComponent getNewPasswordConfirm() {
		return newPasswordConfirm;
	}
	
	public void setNewPasswordConfirm(UIComponent newPasswordConfirm) {
		this.newPasswordConfirm = newPasswordConfirm;
	}
	
	public UIComponent getPasswordChangeForm() {
		return passwordChangeForm;
	}
	
	public void setPasswordChangeForm(UIComponent passwordChangeForm) {
		this.passwordChangeForm = passwordChangeForm;
	}
	
	public void login() {
		try {
			BreweryUser user = em.createQuery("SELECT u FROM BreweryUser u WHERE u.email = :email", BreweryUser.class)
				.setParameter("email", getEmail()).getSingleResult();
			if (user == null) { throw new NoResultException(); }
			
			if (BCrypt.checkpw(password, user.getPassHash())) {
				loginSuccess(user);
			} else {
				addLoginFailMessage();
			}
		} catch (NoResultException nre) {
			try { Thread.sleep(500 + (long)Math.floor(Math.random() * 500)); } catch (InterruptedException ie) { }
			addLoginFailMessage();
		}
		
		passwordChange = new PasswordChange();
	}
	
	public void logout() {
		email = null;
		password = null;
		session.setLoggedInUser(null);
		passwordChange = new PasswordChange();
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void changePassword() {
		BreweryUser user = em.find(BreweryUser.class, session.getLoggedInUser().getId());
		if (user == null) {
			addPasswordChangeMessage("Error updating user password", FacesMessage.SEVERITY_ERROR);
			logout();
		} else {
			if (passwordChange.getNewPassword().equals(passwordChange.getNewPasswordConfirm()) == false) {
				FacesContext.getCurrentInstance().addMessage(newPasswordConfirm.getClientId(),
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Must match New Password", null));
			} else {
				if (BCrypt.checkpw(passwordChange.getOldPassword(), user.getPassHash())) {
					String newHash = BCrypt.hashpw(passwordChange.getNewPassword(), BCrypt.gensalt(12));
					user.setPassHash(newHash);
					em.flush();
					
					addPasswordChangeMessage("Password Changed.", FacesMessage.SEVERITY_INFO);
					
					passwordChange = new PasswordChange();
				} else {
					addPasswordChangeMessage("Old password incorrect, please try again.", FacesMessage.SEVERITY_ERROR);
				}
			}
		}
	}
	
	public void cancelChangePassword() {
		passwordChange = new PasswordChange();
	}
	
	protected void addLoginFailMessage() {
		addLoginFailMessage("Login failed, Please try again.");
	}
	
	protected void addLoginFailMessage(String message) {
		FacesContext.getCurrentInstance().addMessage(authForm.getClientId(),
				new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
		log.error("Login failure for " + (email != null ? email : "NULL"));
	}
	
	protected void addPasswordChangeMessage(String message, Severity severity) {
		FacesContext.getCurrentInstance().addMessage(passwordChangeForm.getClientId(),
				new FacesMessage(severity, message, null));
	}
	
	protected void loginSuccess(BreweryUser user) {
		log.info("Login: " + email);
		
		em.detach(user);
		user.setPassHash(null);
		password = null;
		
		session.setLoggedInUser(user);
	}
}
