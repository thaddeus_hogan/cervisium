package com.cervisium.webapp.bean;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;

import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.FilenameUtils;
import com.cervisium.webapp.Navigation;
import com.cervisium.webapp.PhotoFileException;
import com.cervisium.webapp.PhotoFileLocation;
import com.cervisium.webapp.Resources;
import com.cervisium.webapp.entity.Album;
import com.cervisium.webapp.entity.Photo;
import com.cervisium.webapp.services.ImageService;
import com.cervisium.webapp.services.PhotoDeleteService;

@ViewScoped
@Named("albumEditor")
public class AlbumEditorBean implements Serializable {
	public static final long serialVersionUID = 1l;
	
	@Inject @Cervisium private transient EntityManager em;
	@Inject @Cervisium private transient Logger log;
	@Inject private SessionState session;
	@Inject @PhotoFileLocation File photoFileLocation;
	
	@Inject private transient ImageService imageService;
	@Inject private transient PhotoDeleteService deleteService;
	
	private Integer editAlbumId;
	private Album editedAlbum;
	private String selectedPhotoIdStr;
	private Photo selectedPhoto;
	private Integer resizeMaxDim;
	
	public Integer getEditAlbumId() { return editAlbumId; }
	public void setEditAlbumId(Integer editAlbumId) { this.editAlbumId = editAlbumId; }
	public Album getEditedAlbum() { return editedAlbum; }
	public void setEditedAlbum(Album editedAlbum) { this.editedAlbum = editedAlbum; }
	public String getSelectedPhotoIdStr() { return selectedPhotoIdStr; }
	public void setSelectedPhotoIdStr(String selectedPhotoIdStr) { this.selectedPhotoIdStr = selectedPhotoIdStr; }
	public Photo getSelectedPhoto() { return selectedPhoto; }
	public void setSelectedPhoto(Photo selectedPhoto) { this.selectedPhoto = selectedPhoto; }
	public Integer getResizeMaxDim() { return resizeMaxDim; }
	public void setResizeMaxDim(Integer resizeMaxDim) { this.resizeMaxDim = resizeMaxDim; }
	
	public String getPhotoURLPrefix() {
		return Resources.getPhotoURLPrefix();
	}
	
	public void loadAlbum() {
		if (editAlbumId < 0) {
			editedAlbum = new Album();
			editedAlbum.setBrewery(session.getActiveBrewery());
		} else {
			editedAlbum = em.find(Album.class, editAlbumId);
		}
		
		selectedPhotoIdStr = null;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveAlbum() throws PhotoFileException {
		if (getWritable() == false) { return; }
		
		boolean newAlbum = false;
		
		if (editedAlbum.getId() == null) {
			newAlbum = true;
			
			editedAlbum.generateFolderName();
			em.persist(editedAlbum);
			em.flush();
			editedAlbum.generateFolderName();
			em.flush();
			
			File newAlbumFolder = new File(photoFileLocation, editedAlbum.getFolder());
			if (newAlbumFolder.isDirectory() == false) { 
				newAlbumFolder.mkdir();
			}
			
			if (newAlbumFolder.isDirectory() == false) {
				String err = "Failed to create album directory: " + newAlbumFolder.getAbsolutePath();
				log.error(err);
				throw new PhotoFileException(err);
			}
			
			em.detach(editedAlbum);
		} else {
			em.merge(editedAlbum);
		}
		
		FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage(FacesMessage.SEVERITY_INFO, "Photo album details saved.", null));
		
		if (newAlbum) { Navigation.navigate("/photo/album?faces-redirect=true&id=" + Integer.toString(editedAlbum.getId())); }
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void fileUploadListener(FileUploadEvent event) throws PhotoFileException {
		if (editedAlbum.getId() == null || getWritable() == false) { return; }
		
		// Prepare a record for the new photo
		Photo photo = new Photo();
		photo.setAlbum(editedAlbum);
		photo.setPhotoTime(new Date());
		
		String newFileName = FilenameUtils.simplifyFilename(event.getFile().getFileName());
		photo.setFileName(newFileName);
		
		File albumFolder = new File(photoFileLocation, editedAlbum.getFolder());
		File photoFile = new File(albumFolder, newFileName);
		FileOutputStream photoOut = null;
		
		try {
			photoOut = new FileOutputStream(photoFile);
			IOUtils.copy(event.getFile().getInputstream(), photoOut);
		} catch (IOException ioe) {
			String err = "Failed to save photo file: " + photoFile.getAbsolutePath();
			log.error(err, ioe);
			throw new PhotoFileException(err, ioe);
		} finally {
			if (photoOut != null) { try { photoOut.close(); } catch (IOException ioe2) {} }
		}
		
		// File saved okay, persist the photo
		em.persist(photo);
		em.flush();
		
		imageService.generateThumb(photo);
		
		loadAlbum();
	}
	
	public void rethumbAlbum() {
		if (getWritable() == false) { return; }
		FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage(FacesMessage.SEVERITY_INFO, "Initiated background regeneration of thumbnails.", null));
		imageService.rethumbAlbum(editedAlbum);
	}
	
	public void photoClickListener() {
		Integer selectedPhotoId = Integer.parseInt(selectedPhotoIdStr);
		selectedPhoto = em.find(Photo.class, selectedPhotoId);
	}
	
	public void saveSelectedPhoto() {
		if (getWritable() == false) { return; }
		em.merge(selectedPhoto);
		FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage(FacesMessage.SEVERITY_INFO, "Photo saved.", null));
	}
	
	public void resizeSelectedPhoto() {
		if (getWritable() == false) { return; }
		if (resizeMaxDim == null || resizeMaxDim < 100) {
			FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Resize max dimension too small, must be greater than 100", null));
			return;
		}
		
		imageService.resizePhoto(selectedPhoto, resizeMaxDim);
		FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage(FacesMessage.SEVERITY_INFO, "Photo resized.", null));
	}
	
	public void deleteSelectedPhoto() {
		if (getWritable() == false) { return; }
		deleteService.deletePhoto(selectedPhoto);
		loadAlbum();
	}
	
	public String deleteAlbum() {
		if (getWritable() == false) { return ""; }
		deleteService.deleteAlbum(editedAlbum);
		return "/photo/manage?faces-redirect=true";
	}
	
	public boolean getWritable() {
		if (session.getLoggedIn() == false || session.getLoggedInUser().getPrivManagePhotos() == false) { return false; }
		if (editedAlbum.getBrewery().getId() != session.getLoggedInUser().getBrewery().getId()) { return false; }
		
		return true;
	}
}