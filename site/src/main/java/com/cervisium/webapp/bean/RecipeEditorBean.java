package com.cervisium.webapp.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.bean.model.IntegerKeyedModel;
import com.cervisium.webapp.entity.Brewer;
import com.cervisium.webapp.entity.Formulation;
import com.cervisium.webapp.entity.Recipe;
import com.cervisium.webapp.services.RecipeDeleteService;

@Named("recipeEditor")
@ViewScoped
public class RecipeEditorBean implements Serializable {

	public static final long serialVersionUID = 1l;
	
	@Inject @Cervisium private transient Logger log;
	@Inject @Cervisium private transient EntityManager em;
	@Inject private SessionState session;
	
	@Inject RecipeDeleteService deleteService;
	
	private Integer editRecipeId;
	private Recipe editedRecipe;
	
	private List<Brewer> brewers;
	private Integer selectedAddAuthor;
	private Integer selectedRemoveAuthor;
	private SelectItem noBrewerSelectionItem = new SelectItem(new Integer(-1), "Select Brewer...");
	
	@PostConstruct
	public void init() {
		brewers = em.createQuery("SELECT b FROM Brewer b WHERE b.breweryId = :breweryId", Brewer.class)
			.setParameter("breweryId", session.getActiveBrewery().getId())
			.getResultList();
		selectedAddAuthor = -1;
	}
	
	public Recipe getEditedRecipe() { return editedRecipe; }
	public void setEditedRecipe(Recipe editedRecipe) { this.editedRecipe = editedRecipe; }
	public Integer getEditRecipeId() { return editRecipeId; }
	public void setEditRecipeId(Integer editRecipeId) { this.editRecipeId = editRecipeId; }
	public List<Brewer> getBrewers() { return brewers; }
	public Integer getSelectedAddAuthor() { return selectedAddAuthor; }
	public void setSelectedAddAuthor(Integer selectedAddAuthor) { this.selectedAddAuthor = selectedAddAuthor; }
	public Integer getSelectedRemoveAuthor() { return selectedRemoveAuthor; }
	public void setSelectedRemoveAuthor(Integer selectedRemoveAuthor) { this.selectedRemoveAuthor = selectedRemoveAuthor; }
	public SelectItem getNoBrewerSelectionItem() { return noBrewerSelectionItem; }
	
	public boolean getWritable() {
		if (session.getLoggedIn()) {
			if (session.getLoggedInUser().getBrewery().getId() == editedRecipe.getBreweryId()) {
				return true;
			}
		}
		
		return false;
	}
	
	public List<SelectItem> getBrewerItems() {
		ArrayList<SelectItem> items = new ArrayList<SelectItem>();
		for (Brewer b : getBrewers()) {
			if (editedRecipe.getAuthors().contains(b) == false) {
				items.add(new SelectItem(b.getId(), b.getName()));
			}
		}
		return items;
	}
	
	public void addSelectedAuthor() {
		if (selectedAddAuthor != null && selectedAddAuthor >= 0) {
			Brewer author = em.find(Brewer.class, selectedAddAuthor);
			if (editedRecipe.getAuthors().contains(author) == false) {
				editedRecipe.getAuthors().add(author);
			}
		}
		
		selectedAddAuthor = -1;
	}
	
	public void loadRecipe() {
		if (editRecipeId == -1) {
			editedRecipe = new Recipe();
			editedRecipe.setBreweryId(session.getActiveBrewery().getId());
		} else {
			editedRecipe = em.find(Recipe.class, editRecipeId);
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveRecipe() {
		if (getWritable() == false) { return; }
		
		if (editedRecipe.getId() == null) {
			em.persist(editedRecipe);
		}
		
		em.merge(editedRecipe);
		
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Recipe Saved", null));
	}
	
	public String deleteRecipe() {
		if (getWritable() == false) { return ""; }
		
		deleteService.deleteRecipe(editedRecipe);
		return "/recipes/index?faces-redirect=true";
	}
	
	public void removeAuthor() {
		if (selectedRemoveAuthor != null) {
			Brewer brewerToRemove = null;
			for (Brewer b : editedRecipe.getAuthors()) {
				if (b.getId() == selectedRemoveAuthor) { brewerToRemove = b; }
			}
			
			if (brewerToRemove != null) { editedRecipe.getAuthors().remove(brewerToRemove); }
		}
	}
}
