package com.cervisium.webapp.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.cervisium.webapp.FilenameUtils;

@Entity
@Table(name = "FORMULATION")
public class Formulation implements IntegerKeyedEntity {
	
	private Integer id;
	private String name;
	private String description;
	private Integer targetSizeGallons;
	private String filePath;
	private Recipe recipe;
	private List<Brewing> brewings = new ArrayList<Brewing>();
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "NAME")
	@NotNull @Size(min = 1, max = 255)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "DESCRIPTION")
	@Size(max = 255, message = "Max description length is 255 characters")
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "TARGET_SIZE_GALLONS")
	@NotNull(message = "A target size must be specified")
	public Integer getTargetSizeGallons() {
		return targetSizeGallons;
	}

	public void setTargetSizeGallons(Integer targetSizeGallons) {
		this.targetSizeGallons = targetSizeGallons;
	}

	@Column(name = "FILE_PATH")
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@ManyToOne
	@JoinColumn(name = "RECIPE_ID", referencedColumnName = "ID", nullable = false)
	public Recipe getRecipe() {
		return recipe;
	}

	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}
	
	@OneToMany(mappedBy = "formulation", cascade = { CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH })
	public List<Brewing> getBrewings() {
		return brewings;
	}
	
	public void setBrewings(List<Brewing> brewings) {
		this.brewings = brewings;
	}
	
	@Transient @JsonIgnore
	public String createFriendlyFileName() {
		String friendlyName = recipe.getName() + "-" + name;
		return FilenameUtils.simplifyFilename(friendlyName);
	}
}
