package com.cervisium.webapp.entity;

public interface IntegerKeyedEntity {
	public Integer getId();
}
