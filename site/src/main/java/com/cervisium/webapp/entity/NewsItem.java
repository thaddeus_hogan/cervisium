package com.cervisium.webapp.entity;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "NEWS_ITEM")
public class NewsItem implements IntegerKeyedEntity {

	private Integer id;
	private Integer breweryId;
	private Date postDate = new Date();
	private String heading;
	private String contents;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "BREWERY_ID")
	@NotNull
	public Integer getBreweryId() {
		return breweryId;
	}
	
	public void setBreweryId(Integer breweryId) {
		this.breweryId = breweryId;
	}

	@Column(name = "POST_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	@Column(name = "HEADING")
	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	@Column(name = "CONTENTS")
	@Basic(fetch = FetchType.LAZY)
	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}
}
