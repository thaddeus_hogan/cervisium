package com.cervisium.webapp.entity;

public enum BrewStage {

	PRIMARY("Primary", 50),
	SECONDARY("Secondary", 40),
	KEG("Keg", 30),
	BOTTLES("Bottles", 20),
	FINISHED("Finished", 10);
	
	private String stageName;
	private int orderPoints;
	
	private BrewStage(String stageName, int orderPoints) {
		this.stageName = stageName;
		this.orderPoints = orderPoints;
	}
	
	public String getStageName() {
		return stageName;
	}
	
	public int getOrderPoints() {
		return orderPoints;
	}
}
