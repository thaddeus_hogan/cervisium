package com.cervisium.webapp.entity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "BREWERY_USER")
public class BreweryUser implements IntegerKeyedEntity {
	
	private Integer id;
	private String email;
	private String passHash;
	private Brewery brewery;
	private String resetCode;
	private Boolean privUserAdmin = Boolean.FALSE;
	private Boolean privPostNews = Boolean.TRUE;
	private Boolean privManagePhotos = Boolean.TRUE;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@NotNull(message = "Email must be provided")
	@Email(message = "Must be a valid email address")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@NotNull
	public String getPassHash() {
		return passHash;
	}

	public void setPassHash(String passHash) {
		this.passHash = passHash;
	}
	
	@Column(name = "RESET_CODE")
	public String getResetCode() {
		return resetCode;
	}
	
	public void setResetCode(String resetCode) {
		this.resetCode = resetCode;
	}
	
	public void unsetResetCode() {
		resetCode = null;
	}
	
	public void generateResetCode() {
		resetCode = UUID.randomUUID().toString();
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "BREWERY_ID", referencedColumnName = "ID", nullable = false)
	public Brewery getBrewery() {
		return brewery;
	}

	public void setBrewery(Brewery brewery) {
		this.brewery = brewery;
	}

	@Column(name = "PRIV_USER_ADMIN")
	@NotNull
	public Boolean getPrivUserAdmin() {
		return privUserAdmin;
	}
	
	public void setPrivUserAdmin(Boolean privUserAdmin) {
		this.privUserAdmin = privUserAdmin;
	}
	
	@Column(name = "PRIV_POST_NEWS")
	@NotNull
	public Boolean getPrivPostNews() {
		return privPostNews;
	}
	
	public void setPrivPostNews(Boolean privPostNews) {
		this.privPostNews = privPostNews;
	}
	
	@Column(name = "PRIV_MANAGE_PHOTOS")
	@NotNull
	public Boolean getPrivManagePhotos() {
		return privManagePhotos;
	}
	
	public void setPrivManagePhotos(Boolean privManagePhotos) {
		this.privManagePhotos = privManagePhotos;
	}
}
