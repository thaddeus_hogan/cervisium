package com.cervisium.webapp.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.cervisium.webapp.FilenameUtils;

@Entity
@Table(name = "ALBUM")
public class Album implements IntegerKeyedEntity {
	
	private Integer id;
	private Brewery brewery;
	private String name;
	private String folder;
	private Date addedTime = new Date();
	private List<Photo> photos = new ArrayList<Photo>();
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name = "BREWERY_ID", referencedColumnName = "ID", nullable = false)
	public Brewery getBrewery() {
		return brewery;
	}
	
	public void setBrewery(Brewery brewery) {
		this.brewery = brewery;
	}

	@Column(name = "NAME")
	@Size(min = 1, max = 80, message = "Album name mus be between 1 and 80 characters long")
	@NotNull
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "FOLDER")
	@Size(min = 1, max = 40, message = "Album folder name must be between 1 and 40 characters long")
	@Pattern(regexp = "[a-zA-Z0-9_\\-\\.]+", message = "Folder name may contain only alpha-numeric charaters and _ - .")
	@NotNull
	public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}
	
	@Column(name = "ADDED_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	public Date getAddedTime() {
		return addedTime;
	}
	
	public void setAddedTime(Date addedTime) {
		this.addedTime = addedTime;
	}
	
	@OneToMany(mappedBy = "album", cascade = { CascadeType.DETACH })
	public List<Photo> getPhotos() {
		return photos;
	}
	
	public void setPhotos(List<Photo> photos) {
		this.photos = photos;
	}
	
	public void generateFolderName() {
		String simpleName = FilenameUtils.simplifyFilename(name);
		if (id != null) { simpleName = Integer.toString(id) + "-" + simpleName; }
		if (simpleName.length() > 40) { simpleName = simpleName.substring(0, 40); }
		setFolder(simpleName);
	}
}
