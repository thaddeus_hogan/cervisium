package com.cervisium.webapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "BREWER")
public class Brewer implements IntegerKeyedEntity {

	private Integer id;
	private Integer breweryId;
	private String name;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "BREWERY_ID")
	@NotNull
	public Integer getBreweryId() {
		return breweryId;
	}
	
	public void setBreweryId(Integer breweryId) {
		this.breweryId = breweryId;
	}
	
	@Column(name = "NAME")
	@NotNull
	@Size(min = 1, max = 255)
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object obj) {
		return (obj instanceof Brewer && ((Brewer)obj).getId() == id);
	}
	
	@Override
	public int hashCode() {
		return id == null ? 0 : id.hashCode();
	}
}
