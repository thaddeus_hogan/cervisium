package com.cervisium.webapp.entity;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.cervisium.webapp.FilenameUtils;

@Entity
@Table(name = "BREWING")
public class Brewing implements IntegerKeyedEntity {

	private Integer id;
	private Formulation formulation;
	private Date dateBrew;
	private Date dateSecondary;
	private Date dateKeg;
	private Date dateBottle;
	private Date dateFinish;
	private Float og;
	private Float fg;
	private Float mashTempF;
	private Float efficiency;
	private Float attenuationApp;
	private Float attenuationReal;
	private Integer ibu;
	private Integer srm;
	private Float abv;
	private Integer calsPint;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "FORMULATION_ID", referencedColumnName = "ID", nullable = false)
	public Formulation getFormulation() {
		return formulation;
	}

	public void setFormulation(Formulation formulation) {
		this.formulation = formulation;
	}

	@Column(name = "DATE_BREW")
	@Temporal(TemporalType.DATE)
	@NotNull(message = "A brew date must be specified")
	public Date getDateBrew() {
		return dateBrew;
	}

	public void setDateBrew(Date dateBrew) {
		this.dateBrew = dateBrew;
	}

	@Column(name = "DATE_SECONDARY")
	@Temporal(TemporalType.DATE)
	public Date getDateSecondary() {
		return dateSecondary;
	}

	public void setDateSecondary(Date dateSecondary) {
		this.dateSecondary = dateSecondary;
	}

	@Column(name = "DATE_KEG")
	@Temporal(TemporalType.DATE)
	public Date getDateKeg() {
		return dateKeg;
	}

	public void setDateKeg(Date dateKeg) {
		this.dateKeg = dateKeg;
	}

	@Column(name = "DATE_BOTTLE")
	@Temporal(TemporalType.DATE)
	public Date getDateBottle() {
		return dateBottle;
	}

	public void setDateBottle(Date dateBottle) {
		this.dateBottle = dateBottle;
	}

	@Column(name = "DATE_FINISH")
	@Temporal(TemporalType.DATE)
	public Date getDateFinish() {
		return dateFinish;
	}

	public void setDateFinish(Date dateFinish) {
		this.dateFinish = dateFinish;
	}

	@Column(name = "OG")
	public Float getOg() {
		return og;
	}

	public void setOg(Float og) {
		this.og = og;
	}

	@Column(name = "FG")
	public Float getFg() {
		return fg;
	}

	public void setFg(Float fg) {
		this.fg = fg;
	}

	@Column(name = "MASH_TEMP_F")
	public Float getMashTempF() {
		return mashTempF;
	}

	public void setMashTempF(Float mashTempF) {
		this.mashTempF = mashTempF;
	}

	@Column(name = "EFFICIENCY")
	public Float getEfficiency() {
		return efficiency;
	}

	public void setEfficiency(Float efficiency) {
		this.efficiency = efficiency;
	}
	
	@Transient
	public Float getEfficiencyPct() {
		return efficiency == null ? null : Math.round(efficiency * 1000) / 10f;
	}
	
	public void setEfficiencyPct(Float efficiencyPct) {
		efficiency = efficiencyPct / 100f;
	}
	
	@Column(name = "ATTENUATION_APP")
	public Float getAttenuationApp() {
		return attenuationApp;
	}

	public void setAttenuationApp(Float attenuationApp) {
		this.attenuationApp = attenuationApp;
	}
	
	@Transient
	public Float getAttenuationAppPct() {
		return attenuationApp == null ? null : Math.round(attenuationApp * 1000) / 10f;
	}

	public void setAttenuationAppPct(Float attenuationAppPct) {
		this.attenuationApp = attenuationAppPct / 100f;
	}

	@Column(name = "ATTENUATION_REAL")
	public Float getAttenuationReal() {
		return attenuationReal;
	}

	public void setAttenuationReal(Float attenuationReal) {
		this.attenuationReal = attenuationReal;
	}
	
	@Transient
	public Float getAttenuationRealPct() {
		return attenuationReal == null ? null : Math.round(attenuationReal * 1000) / 10f;
	}

	public void setAttenuationRealPct(Float attenuationRealPct) {
		this.attenuationReal = attenuationRealPct / 100f;
	}

	@Column(name = "IBU")
	public Integer getIbu() {
		return ibu;
	}

	public void setIbu(Integer ibu) {
		this.ibu = ibu;
	}

	@Column(name = "SRM")
	public Integer getSrm() {
		return srm;
	}

	public void setSrm(Integer srm) {
		this.srm = srm;
	}

	@Column(name = "ABV")
	public Float getAbv() {
		return abv;
	}

	public void setAbv(Float abv) {
		this.abv = abv;
	}
	
	@Transient
	public Float getAbvPct() {
		return abv == null ? null : Math.round(abv * 1000) / 10f;
	}

	public void setAbvPct(Float abvPct) {
		this.abv = abvPct / 100f;
	}

	@Column(name = "CALS_PINT")
	public Integer getCalsPint() {
		return calsPint;
	}

	public void setCalsPint(Integer calsPint) {
		this.calsPint = calsPint;
	}
	
	@Transient
	public BrewStage getBrewStage() {
		if (dateFinish == null) {
			if (dateBottle == null) {
				if (dateKeg == null) {
					if (dateSecondary == null) {
						return BrewStage.PRIMARY;
					} else {
						return BrewStage.SECONDARY;
					}
				} else {
					return BrewStage.KEG;
				}
			} else {
				return BrewStage.BOTTLES;
			}
		} else {
			return BrewStage.FINISHED;
		}
	}
	
	@Transient @JsonIgnore
	public String getFriendlyName() {
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		String friendlyName = formulation.getRecipe().getName() + "-" + formulation.getName() + "_" +
			formatter.format(getDateBrew());
		return FilenameUtils.simplifyFilename(friendlyName);
	}
}
