package com.cervisium.webapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "BREWERY")
public class Brewery implements IntegerKeyedEntity {

	private Integer id;
	private String name;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "NAME")
	@NotNull
	@Size(min = 2, max = 255)
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Brewery) {
			if (((Brewery)obj).getId().equals(id)) { return true; }
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return id.hashCode();
	}
}
