package com.cervisium.webapp;

import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.context.FacesContext;

public class Navigation {
	
	public static void navigate(String outcome) {
		ConfigurableNavigationHandler cnh = (ConfigurableNavigationHandler)FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
		cnh.performNavigation(outcome);
	}
}
