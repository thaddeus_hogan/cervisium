package com.cervisium.webapp.btp;

public class Yeast extends Ingredient {

	private String supplier;
	private String catalog;
	private String strain;
	
	public String getSupplier() {
		return supplier;
	}
	
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	
	public String getCatalog() {
		return catalog;
	}
	
	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}
	
	public String getStrain() {
		return strain;
	}
	
	public void setStrain(String strain) {
		this.strain = strain;
	}
	
	@Override
	public void setName(String name) {
		this.strain = name;
	}
	
	@Override
	public String getName() {
		return supplier + " " + catalog + " " + strain;
	}
	
	@Override
	public int getOrderPoints() {
		return 4000;
	}
}
