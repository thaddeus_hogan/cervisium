package com.cervisium.webapp.btp;

import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public abstract class BTPParser {

	protected Document doc;
	
	public BTPParser(Document doc) {
		this.doc = doc;
	}
	
	public abstract List<Ingredient> getIngredients();
	public abstract BrewingData getBrewingData();
	
	public VU parseValueUnitNode(Node qNode) {
		VU data = new VU();
		
		for (Node qParam = qNode.getFirstChild(); qParam != null; qParam = qParam.getNextSibling()) {
			if (qParam.getNodeName().equalsIgnoreCase("value")) {
				try {
					String valueStr = qParam.getTextContent().trim();
					float v = Float.parseFloat((valueStr == null ? "0.0" : valueStr));
					data.value = v;
				} catch (NumberFormatException nfe) {}
			} else if (qParam.getNodeName().equalsIgnoreCase("unit")) {
				data.unit = qParam.getTextContent().trim();
			}
		}
		
		return data;
	}
	
	public class VU {
		public Float value = 0f;
		public String unit = "";
	}
}
