package com.cervisium.webapp.btp;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class BTPData {

	private File dataFile; 
	private Document doc = null;
	private BTPParser parser;
	
	public BTPData(File dataFile) {
		this.dataFile = dataFile;
	}
	
	public void loadDataFile() throws IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = dbf.newDocumentBuilder();
			doc = builder.parse(dataFile);
		} catch (ParserConfigurationException pce) {
			throw new IOException("Failed to create XML document parser", pce);
		} catch (SAXException se) {
			throw new IOException("Failed to parse XML document", se);
		}
		
		// Examine XML document to determine parser to use
		parser = null;
		
		Node root = doc.getDocumentElement();
		if (root.getNodeName().equalsIgnoreCase("Recipe")) {
			String ver = root.getAttributes().getNamedItem("version").getTextContent().trim();
			if (ver != null && ver.startsWith("1.5")) {
				parser = new BTP15Parser(doc);
			}
		} else if (root.getNodeName().equalsIgnoreCase("Session")) {
			String ver = root.getAttributes().getNamedItem("version").getTextContent().trim();
			if (ver != null && ver.startsWith("1.0")) {
				parser = new BTP10Parser(doc);
			}
		}
		
		// We should have a parser by now
		if (parser == null) {
			throw new IOException("Could not determine parser for BTP file at: " + dataFile.getAbsolutePath());
		}
	}
	
	public BTPParser getParser() {
		return parser;
	}
}
