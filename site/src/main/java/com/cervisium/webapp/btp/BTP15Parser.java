package com.cervisium.webapp.btp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class BTP15Parser extends BTPParser {

	public BTP15Parser(Document doc) {
		super(doc);
	}
	
	@Override
	public List<Ingredient> getIngredients() {
		ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();
		
		Node ingredientsParent = null;
		for (Node docNode = doc.getDocumentElement().getFirstChild(); docNode != null; docNode = docNode.getNextSibling()) {
			if (docNode.getNodeName().equalsIgnoreCase("Ingredients")) {
				ingredientsParent = docNode; break;
			}
		}
		
		for (Node iNode = ingredientsParent.getFirstChild(); iNode != null; iNode = iNode.getNextSibling()) {
			Ingredient i;
			// Grain Ingredients
			if (iNode.getNodeName().equalsIgnoreCase("Grain")) {
				Grain grain = new Grain();
				i = grain;
			}
			// Extract Ingredients
			else if (iNode.getNodeName().equalsIgnoreCase("Extract")) {
				Extract extract = new Extract();
				i = extract;
			}
			// Hop Ingredients
			else if (iNode.getNodeName().equalsIgnoreCase("Hop")) {
				Hop hop = new Hop();
				for (Node hopParam = iNode.getFirstChild(); hopParam != null; hopParam = hopParam.getNextSibling()) {
					if (hopParam.getNodeName().equalsIgnoreCase("alpha")) {
						VU alpha = parseValueUnitNode(hopParam);
						hop.setAlphaAcid(alpha.value);
					}
				}
				
				i = hop;
			}
			// Yeast Ingredients
			else if (iNode.getNodeName().equalsIgnoreCase("Yeast")) {
				Yeast yeast = new Yeast();
				yeast.setStage(IngredientStage.FERMENTER);
				
				for (Node yeastParam = iNode.getFirstChild(); yeastParam != null; yeastParam = yeastParam.getNextSibling()) {
					if (yeastParam.getNodeName().equalsIgnoreCase("Supplier")) {
						yeast.setSupplier(yeastParam.getTextContent().trim());
					} else if (yeastParam.getNodeName().equalsIgnoreCase("Catalog")) {
						yeast.setCatalog(yeastParam.getTextContent().trim());
					}
				}
				
				i = yeast;
			}
			// Other Ingredients
			else {
				i = new Ingredient();
			}
			
			// Standard Handling
			for (Node param = iNode.getFirstChild(); param != null; param = param.getNextSibling()) {
				String pName = param.getNodeName();
				if (pName.equalsIgnoreCase("name")) {
					i.setName(param.getTextContent().trim());
				} else if (pName.equalsIgnoreCase("stage")) {
					try {
						String stageIdStr = param.getTextContent().trim();
						int stageId = Integer.parseInt((stageIdStr == null ? "-1" : stageIdStr));
						i.setStage(IngredientStage.forValue(stageId));
					} catch (NumberFormatException nfe) {
						i.setStage(IngredientStage.UNKNOWN);
					}
				} else if (pName.equalsIgnoreCase("quantity")) {
					VU quantity = parseValueUnitNode(param);
					i.setQuantity(quantity.value);
					i.setQuantityUnit(quantity.unit);
				} else if (pName.equalsIgnoreCase("boilduration")) {
					VU boil = parseValueUnitNode(param);
					i.setBoilDuration(boil.value);
					i.setBoilDurationUnit(boil.unit);
				}
			}
			
			// ADD IT
			ingredients.add(i);
		}
		
		Collections.sort(ingredients);
		return ingredients;
	}
	
	@Override
	public BrewingData getBrewingData() {
		BrewingData data = new BrewingData();
		
		for (Node node = doc.getDocumentElement().getFirstChild(); node != null; node = node.getNextSibling()) {
			if (node.getNodeName().equalsIgnoreCase("OGReading")) {
				data.setOg(parseValueUnitNode(node).value);
			} else if (node.getNodeName().equalsIgnoreCase("TGReading")) {
				data.setFg(parseValueUnitNode(node).value);
			} else if (node.getNodeName().equalsIgnoreCase("BrewHouseYield")) {
				data.setEfficiency(parseValueUnitNode(node).value);
			} else if (node.getNodeName().equalsIgnoreCase("ApparentAttenuation")) {
				data.setAttenuationApp(parseValueUnitNode(node).value);
			} else if (node.getNodeName().equalsIgnoreCase("Schedule")) {
				for (Node sNode = node.getFirstChild(); sNode != null; sNode = sNode.getNextSibling()) {
					if (sNode.getNodeName().equalsIgnoreCase("MashIn")) {
						for (Node mNode = sNode.getFirstChild(); mNode != null; mNode = mNode.getNextSibling()) {
							if (mNode.getNodeName().equalsIgnoreCase("TempOut")) {
								data.setMashTempF(parseValueUnitNode(mNode).value);
							}
						}
					}
				}
			}
		}
		
		return data;
	}

}
