package com.cervisium.webapp.btp;

public class Hop extends Ingredient {

	private Float alphaAcid;
	
	public void setAlphaAcid(Float alphaAcid) {
		this.alphaAcid = alphaAcid;
	}
	
	public Float getAlphaAcid() {
		return alphaAcid;
	}
	
	public String getDisplayAlphaAcid() {
		return format("%.1f%%", (getAlphaAcid() * 100));
	}
	
	@Override
	public String getName() {
		return name + " (" + getDisplayAlphaAcid() + ")";
	}
	
	@Override
	public int getOrderPoints() {
		return 3000;
	}
}
