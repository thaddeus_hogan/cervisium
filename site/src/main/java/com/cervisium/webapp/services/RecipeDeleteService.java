package com.cervisium.webapp.services;

import java.io.File;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.RecipeFileLocation;
import com.cervisium.webapp.entity.Brewing;
import com.cervisium.webapp.entity.Formulation;
import com.cervisium.webapp.entity.Recipe;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class RecipeDeleteService {
	@PersistenceContext(unitName = "CervisiumDS") private EntityManager em;
	@Inject @Cervisium private Logger log;
	@Inject @RecipeFileLocation File recipeFileLocation;
	
	public void deleteRecipe(Recipe recipe) {
		if (recipe.getId() != null) {
			recipe = em.merge(recipe);
			
			// Delete any child formulations if they exist
			deleteForumlations(recipe.getFormulations().toArray(new Formulation[0]));
			
			// Delete recipe
			em.createQuery("DELETE FROM Recipe r WHERE r.id = :id")
				.setParameter("id", recipe.getId())
				.executeUpdate();
		}
	}
	
	public void deleteForumlations(Formulation... formulations) {
		for (Formulation formulation : formulations) {
			if (formulation.getId() != null) {
				formulation = em.merge(formulation);
				// Delete formulation file if it exists
				if (formulation.getFilePath() != null) {
					File formulationFile = new File(formulation.getFilePath());
					if (formulationFile.exists()) { formulationFile.delete(); }
				}
				
				// Delete brewings, if any
				deleteBrewings(formulation.getBrewings().toArray(new Brewing[0]));
				
				em.createQuery("DELETE FROM Formulation f WHERE f.id = :id")
					.setParameter("id", formulation.getId()).executeUpdate();
			}
		}
	}
	
	public void deleteBrewings(Brewing... brewings) {
		Integer[] brewingIds = new Integer[brewings.length];
		for (int i = 0; i < brewings.length; i++) {
			if (brewings[i].getId() != null) { brewingIds[i] = brewings[i].getId(); }
			else { brewingIds[i] = -1; }
		}
		
		deleteBrewings(brewingIds);
	}
	
	public void deleteBrewings(Integer... brewingIds) {
		for (Integer id : brewingIds) {
			if (id < 0) { continue; }
			
			File brewingFile = new File(recipeFileLocation, "brewing-" + Integer.toString(id) + ".btp");
			
			// Delete the BTP file if it exists
			if (brewingFile != null && brewingFile.exists()) {
				brewingFile.delete();
			}
			
			em.createQuery("DELETE FROM Brewing b WHERE b.id = :id")
				.setParameter("id", id).executeUpdate();
		}
	}
}
